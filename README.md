# Melpomene

![](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

Melpomene is a a simple, HTML/CSS/JS webcomic reading utility .

It allows people to look through your webcomic like they would in real life : panel by panel. It displays your pages through a series of zooms you defines beforehand.

To avoid writing the zooms informations by hand, Melpomene utility scripts uses SVGs as input to defines the zooms. It allows you to use any SVG editor as a "What You See Is What You Get" editor.

This repository host a small demo you can open in your browser : the [episode 35 of pepper and carrot](https://www.peppercarrot.com/en/webcomic/ep35_The-Reflection.html) (see Credits). You can open the HTML files in the `demos` folder to see Mepomene in action!

Melpomene main repository is [https://git.aribaud.net/caribaud/melpomene](https://git.aribaud.net/caribaud/melpomene)

# How Melpomene works

```mermaid
flowchart TB
    page([Your comic pages]) -- use in --> svgedit[SVG editor<br>e.g. Inkscape, etc...] 
    svgedit -- export --> svg([SVGs with<br>zooms defined])
    svg -- use in --> gen[Melpomene<br>config generator]
    gen -- generate --> html([Melpomene HTML snipet])
    html -- copy into --> webpage([Your web page])
    js([melpomene.js]) & css([melpomene.css]) -- include into --> webpage
```

Melpomene is mainly one JS file and one CSS file. You only need to include them in you web page. They are :
    + `melpomene.js`
    + `melpomene.css`

The JS files expect you to write some very specific HTML tags to be able to work.
To simplify things, you only need to copy-paste the content of `melpomene.html` into your own page and change a few things :
+ `data-pages-width` must be set to your comic pages width in px
+ `data-pages-height` must be set to your comic pages height in px
+ You must duplicate the `img` tag for each of you comic page and :
    + set `url` to the actual URL of your page
    + set `data-zooms` with the zooms information, like so : `<zoom 1 width>, <zoom 1 height>, <zoom 1 x offset>, <zoom 1 y offset>; <zoom 2 width> ...`
        + example : `<img loading="lazy" src="https://www.peppercarrot.com/0_sources/ep35_The-Reflection/hi-res/en_Pepper-and-Carrot_by-David-Revoy_E35P01.jpg" data-zooms="2481.0,1327.1057,0.0,0.0;593.15338,1076.4635,0.0,1364.053;890.72864,491.29874,830.81415,1751.5;2481.0,1078.4192,0.0,1364.053;562.77032,909.44702,102.48115,2491.6567;920.74463,909.44702,698.55927,2491.6567;728.776,909.44702,1652.3695,2491.6567"/>`

Because doing this by hand would be very tedious, Melpomene comes with an helper script that allows generating that HTML for you from SVGs.

## Limitations

The following limitations are known and will be improved upon :

+ Mobile support is currently limited
+ There are some performences issues
+ Your comic pages need to be the same size

# How to setup Melpomene ?

## Defining the zooms

All the zooms must be manually defined by you.
To do so, you can use your favorite SVG editor. If you don't have one, [Inkscape](https://inkscape.org/) is a good one.

To create the zooms for a comic page, what you need to do is :

1. Open your comic page in your editor (make sure to keep the image's original size when importing!)
    * If your software ask, there is no need to actually import the image, you only need to link it.
2. Create a simple rectangle over each zoom you want, in the order you want them to show up
    * You can set the rectangle's color to be translucent so you can still see the page underneath them!
    * If you want to change the zoom order, you can change their order in the layer view of your SVG tool if they support it
3. Once you are done, save the SGV


## Generating the HTML files

Once the SVG for your pages are done, put them in one folder. Then, you can run the zoom generator.

You need to open a terminal and then run :
+ `python zooms_generator.py <path to the SVG folder> html -p <prefix of the img's url> -e <extension of the img's url>`
    + For example, if your comic pages are hosted at `static/comic/page_x.jpg`, the svg must be named `page_x.svg`
    and you must run `python zooms_generator.py <path to the SVG folder> html -p /static/comic/ -e jpg`
    + It will generate the following `img` tag : `<img loading="lazy" src="static/comic/page_x.jpg" data-zooms="..."/>`

The pages need to be in alphabetical order! It assumes the first page is page 1, the next one is page 2, etc..

The script will then generate the file `melpomene_data.html`.

If you wish to run a custom generation process, this generator can output a JSON or a JS file as well, run `python zooms_generator.py -h` for help

You are now ready to integrate Melpomene in your website!


# Credits

Most examples and the documentation of Melpomene uses illustrations from David "Deevad" Revoy's "Pepper & Carrot" webcomic, which is published under CC-BY 4.0. Full licence [here](https://www.peppercarrot.com/en/license/index.html).